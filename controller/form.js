const { Cars } = require("../models");

async function form(req, res) {
    let id = req.params.id;
    let title = "Add Car";
    if (id) {
      title = "Update Car Information";
    }
  
    res.status(200).render("form", { 
        layout: 'layouts/main-layout',
        title: title, 
        id: id 
    });
  }
  async function form(req, res) {
    let id = req.params.id;
    let decider = false;
    let realcar;
    if (id) {
      let car = await Cars.findOne({ where: { id: id } });
      realcar = car;
      decider = true;
    } else {
      decider = false;
    }
  
    let title = "Add Car";
    let name, price, size_id;
    if (decider) {
      title = "Update Car Information";
      name = realcar.name;
      price = realcar.price;
      size_id = realcar.size_id;
    }
  
    res.status(200).render("form", {
      layout: 'layouts/main-layout',
      title: title,
      id: id,
      name: name,
      price: price,
      size_id: size_id,
    });
  }
  
  module.exports = form;
  