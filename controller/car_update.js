const { Cars } = require("../models");

async function carUpdate(req,res){
    let iid = req.params.id;
    let arrayId = [];
    let splitId = iid.split("");

    for (let i in splitId) {
        if (splitId[i] != ":") {
        arrayId.push(splitId[i]);
        }
    }
    let finalId = arrayId.join("");

    await Cars.update(
        {
            name : req.body.name,
            price : req.body.price,
            size_id : req.body.size_id,
            photo : req.body.photo
        },
        {where :{id:finalId}}
    );
    res.send(`Data berhasil diupdate`)
}

module.exports = carUpdate;