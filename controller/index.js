module.exports = {
    carGet: require("./car_get"),
    carGetAll: require("./car_findAll"),
    carPost: require("./car_post"),
    carPut: require("./car_update"),
    carDelete: require("./car_delete"),
    carGetById: require("./car_getById"),
    carUpload: require("./car_upload"),
    form: require("./form")
  };
  