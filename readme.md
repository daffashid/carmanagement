# Challenge 5 Car Crud Management

Challenge kali ini membuat desain web car management dengan sistem CRUD (Create, Read, Update, Delete) yang menggunakan third party module express.js sebagai server dan pembuatan API. Sistem ini juga sudah terhubung ke dalam database Postgresql.

## Tech

Di challenge kali ini banyak menggunakan teknologi yang berbeda dari challenge sebelumnya yaitu :

- [Express.js 4.17.3](http://expressjs.com/).
- [ejs ^3.1.7](https://ejs.co/)
- [express-ejs-layouts ^2.5.1](https://www.npmjs.com/package/express-ejs-layouts)
- [multer ^1.4.4](https://www.npmjs.com/package/multer)
- [pg ^8.7.3](https://www.npmjs.com/package/pg)
- [pg-hstore ^2.3.4](https://www.npmjs.com/package/pg-hstore)
- [sequelize ^6.19.0](https://sequelize.org/)
- [sequelize-cli ^6.4.1](https://www.npmjs.com/package/sequelize-cli)

## Running Steps

- Lakukan clone dari repository project ini.
- Setelah selesai proses cloning dan sudah bisa membuka project ini, Jalankan command npm install atau yarn install.
- Setelah terinstall kita masuk ke dalam folder config untuk mengganti username dan password database.
- Setelah itu kita akan membuat database dengan command npx sequelize db:create.
- Lalu setelah database dibuat kita akan membuat table yang ada di dalam folder migration dengan command npx sequelize db:migrate.
- Setelah database dan table dibuat pastikan kalau database dan table berhasil dibuat melalui terminal psql atau aplikasi pgadmin.
- Kalau sudah aplikasi bisa dirun dengan command nodemon start atau npm start.
